package ro.infoiasi.pa.shapes;

/**
 * Interface for a geometric shape.
 */
public interface Shape {
	/**
	 * Computes the perimeter of this geometric shape.
	 * @return a double, representing the perimeter's computed valued
	 */
	double getPerimeter();
	
	/**
	 * Computes the area of this geometric shape.
	 * @return a double, representing the area's computed valued
	 */
	double getArea();
}
