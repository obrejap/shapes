package ro.infoiasi.pa.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Polygon implements Shape{
	protected List<Double> edges;
	
	public Polygon(Double... edges){
		this.edges = new ArrayList<Double>();
		this.edges.addAll(Arrays.asList(edges));
	}

	public double getPerimeter() {
		double perimeter = 0D;
		for(Double edge:edges){
			perimeter += edge;
		}
		return perimeter;
	}
	
	//one method, getArea(), is not implemented, so this class must be 'abstract'
}
