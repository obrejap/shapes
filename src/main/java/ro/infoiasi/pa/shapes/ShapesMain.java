package ro.infoiasi.pa.shapes;

public class ShapesMain {
	public static void main(String[] args){
		Shape shape = new Circle(2.0);
		Circle circle = new Circle(3.0);
		System.out.printf("Perimeter for shape is: %.4f%n", shape.getPerimeter());
		System.out.printf("Perimeter for circle is: %.4f%n", circle.getPerimeter());
		System.out.printf("Area for shape is: %.4f%n", shape.getArea());
		System.out.printf("Area for circle is: %.4f%n", circle.getArea());		
		
		int rollingTimes = 5;
/*		System.out.println("shape1 rolled %d times and covered %.4f%n", 
  				rollingTimes, shape1.roll(rollingTimes));
  				(a Shape can't roll) */
		System.out.printf("circle1 rolled %d times and covered %.4f%n",
				rollingTimes, circle.roll(rollingTimes));		
		
		Shape anotherShape = new Shape(){  //anonymous class
			public double getPerimeter() {
				return 40D;
			}

			public double getArea() {
				return 80.12;
			}			
		};
		printShape("anotherShape", anotherShape);
		
		
//		Shape polygon = new Polygon(); //can't instantiate an abstract class
		Shape rectangle = new Rectangle(4.0D, 2.0D);
		printShape("rectangle", rectangle);
		
		Shape square = new Square(5.0D);
		printShape("square", square);
		
		//why not adding a getName() method in Shape interface and use it while displaying the shape?
	}
	
	private static void printShape(String shapeName, Shape shape){
		System.out.printf("Computed values for %s are:%n", shapeName);
		System.out.printf("%-10s %.4f%n", "Perimeter:", shape.getPerimeter());
		System.out.printf("%-10s %.4f%n", "Area:", shape.getArea());
	}
}
