package ro.infoiasi.pa.shapes;

public class Square extends Rectangle{

	public Square(double length){
		super(length, length); //calls constructor from parent class, Rectangle
	}
	
	//getPerimeter() method is inherited from parent Polygon class
	//getArea() method is inherited from parent Rectangle class
}
