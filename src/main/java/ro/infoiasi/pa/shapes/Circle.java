package ro.infoiasi.pa.shapes;

public class Circle implements Shape{
	
	private double radix;
	
	public Circle(double radix){
		this.radix = radix;
	}	

	public double getPerimeter() {
		return 2 * Math.PI * radix;
	}

	public double getArea() {
		return Math.PI * radix * radix;
	}
	
	public double roll(int times) {
		return times * getPerimeter();
	}
}
