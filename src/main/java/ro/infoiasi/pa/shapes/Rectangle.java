package ro.infoiasi.pa.shapes;

public class Rectangle extends Polygon{
	
	public Rectangle(double length, double width){
		super(length, width, length, width);
	}

	/*getPerimeter() method is inherited from parent Polygon class
	but Rectangle can overwrite it*/
	/*public double getParameter(){
		return 2*(edges.get(0) *  edges.get(1));
	}*/
	
	//getArea() must be implemented, because parent class doesn't provide an implementation
	public double getArea() {
		return edges.get(0) *  edges.get(1);
	}
	


}
